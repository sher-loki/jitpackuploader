import com.android.ide.common.gradle.RELEASE

@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.kotlinAndroid)
    id("maven-publish")
}

android {
    namespace = "com.example.mylibrary"
    compileSdk = 34

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(libs.core.ktx)
    implementation(libs.appcompat)
    implementation(libs.material)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.test.ext.junit)
    androidTestImplementation(libs.espresso.core)
}

afterEvaluate {
    publishing {
        publications {
            // Creates a Maven publication called "release".
            register<MavenPublication>("release") {
                // Applies the component for the release build variant.
                groupId = "com.gitlab.sher-loki"
                artifactId = "jitpack-uploader"
                version = "1.0.0"
                afterEvaluate {
                    from(components["release"])
                }
                // You can then customize attributes of the publication as shown below.
            }
            // Creates a Maven publication called “debug”.
//            register<MavenPublication>("debug") {
//                // Applies the component for the debug build variant.
//                groupId = "com.gitlab.sher-loki"
//                artifactId = "jitpack-uploader"
//                version = "1.0.0"
//                afterEvaluate {
//                    from(components["debug"])
//                }
//            }
        }
    }
}


